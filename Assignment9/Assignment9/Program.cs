﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment9
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] item_price = new double[5] { 15.50, 50.99, 25.00, 115.49, 75.25 };
            string[] item_name = new string[5] { "Widget", "Thingy", "Ratchet", "Clanger", "Fracker" };

            printit(item_name, item_price);

            Console.Write("Enter the price cutoff point (eg $15.00) ");
            var priceCutOffStr = Console.ReadLine();
            var priceCutOff = double.Parse(priceCutOffStr.Replace("$", ""));

            Console.Write("Enter the percentage price change (eg 0.25) ");
            var percentageStr = Console.ReadLine();
            var percentage = double.Parse(percentageStr);

            changePrices(item_price, priceCutOff, percentage);

            printit(item_name, item_price);

            Console.ReadLine();
        }
        static void changePrices(double[] item_price, double priceCutOff, double percentage)
        {
            for (int i = 0; i < 5; i++)
            {
                if (item_price[i]<priceCutOff)
                {
                    item_price[i] += item_price[i] * percentage;
                }
            }
        }
        static void printit(string[] item_name, double[] item_price)
        {
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine($"The price for item {item_name[i]} is {item_price[i].ToString("c2")}");
            }
        }
    }
}
