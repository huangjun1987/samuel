﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment7
{
    class Program
    {
        static void Main(string[] args)
        {
            int randomvalue = 0;
            double average=0;
            double total=0;
            double input=0;

            for (int i = 0; i < 20; i++)
            {
                randomvalue += getRandom();
            }
            calculate(randomvalue, ref average, 20);
            Console.WriteLine($"The average of the 20 random numbers is {average}");

            for (int i = 0; i < 5; i++)
            {
                input = EnterDouble();
                calculate(input, ref total);
            }
            Console.WriteLine($"The total is {total}");

            Console.ReadLine();
        }

        private static void calculate(double input, ref double total)
        {
            total += input;
        }

        private static double EnterDouble()
        {
            Console.Write("Enter a double value ");
            var valueStr = Console.ReadLine();
            if (double.TryParse(valueStr, out double value))
            {
                return value;
            }
            else
            {
                Console.WriteLine("Ivalid entry. Please try again!");
                return EnterDouble();
            }
        }
        private static void calculate(int randomvalue, ref double average, int count)
        {
            average = randomvalue / count;
        }

        private static int getRandom()
        {
            Random rand = new Random();
            return rand.Next(1, 100);
        }
    }
}
