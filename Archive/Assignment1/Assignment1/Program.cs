﻿/*assignment 1a
 * Write an application that reads three integers, adds all three together and computes an 
 * average of the three entries and computes any remainder of the integer division.
 * A remainder represents the modulus result of dividing an even by an odd number or 
 * vice versa.  Display the output.
 * The output will look something like this:
 * Enter an integer score 3
 * Enter an integer score 6
 * Enter an integer score 4
 * The average of 3, 6, 4 is 4 with a remainder of 1
 * Press any key to continue . . .
*/
using System;

namespace Assignment1
{
    class Program
    {
        static void Main(string[] args)
        {
            int score1 = EnterScore();
            int score2 = EnterScore();
            int score3 = EnterScore();

            var sum = score1 + score2 + score3;
            var average = sum / 3;
            var remainder = sum % 3;
            Console.WriteLine($"The average of {score1}, {score2}, {score3} is {average} with a remainder of {remainder}");
            Console.ReadKey();
        }

        private static int EnterScore()
        {
            Console.Write("Enter an integer score ");
            var scoreStr = Console.ReadLine();
            if (int.TryParse(scoreStr, out int score))
            {
                return score;
            }
            else
            {
                Console.WriteLine("Ivalid entry. Please try again!");
                return EnterScore();
            }
        }
    }
}
