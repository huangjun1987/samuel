﻿using System;

namespace Lazarus_assingment_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 2;
            int b = 1;
            int c = a + b;
            Console.WriteLine(c);

            int num1 = 3;
            int num2 = 3;
            int y = num1 + num2;
            Console.WriteLine(y);

            int d = 5;
            int f = 4;
            int e = d + f;
            Console.WriteLine(e);

            int z = (c + y + e) % e;
            Console.WriteLine($"remainder: {z}");



       
            

             
        }
    }
}
/* 0 of 50.  This is not the assignment and I have had at least one other student
submit exactly the same code.  This code comes from an outside source an d was 
not written by you.  Don't do this again.  See solution below.
*/

/*assignment 1a
 * Write an application that reads three integers, adds all three together and computes an 
 * average of the three entries and computes any remainder of the integer division.
 * A remainder represents the modulus result of dividing an even by an odd number or 
 * vice versa.  Display the output.
 * The output will look something like this:
 * Enter an integer score 3
 * Enter an integer score 6
 * Enter an integer score 4
 * The average of 3, 6, 4 is 4 with a remainder of 1
 * Press any key to continue . . .
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CIS243_assignment1a
{
    class Program
    {
        static void Main(string[] args)
        {
            //declare required integer variables
            int score1;
            int score2;
            int score3;
            int average;
            int remainder;

            //take three integer entries from the console
            Console.Write("Enter an integer score ");
            score1 = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter an integer score ");
            score2 = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter an integer score ");
            score3 = Convert.ToInt32(Console.ReadLine());

            //calculate the average of the three scores
            average = (score1 + score2 + score3) / 3;
            //calculate the remainder for the integer division
            remainder = (score1 + score2 + score3) % 3;

            //display the results to the console
            Console.WriteLine("The average of " + score1
                + ", " + score2 + ", " + score3 + " is "
                + average + " with a remainder of " + remainder);

            Console.ReadLine();

        }
    }
}
