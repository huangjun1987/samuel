﻿// Assignment 2a
// average class with automatic properties for the score1, score2 and score3.
using System;

public class average
{
    //You do not have to implement the automatic properties, use these.
    //
    // auto-implemented property Score1 implicitly creates an
    // instance variable for the score1
    public int Score1 { get; set; }

    // auto-implemented property Score2 implicitly creates an
    // instance variable for the score2
    public int Score2 { get; set; }

    // auto-implemented property Score3 implicitly creates an
    // instance variable for the score3
    public int Score3 { get; set; }

    //declare the remainder and avg variables (use these)
    int remainder;
    int avg;

    // constructor shell accepting three values, you fill in the 
    //rest of the code
    public average(int score1Value, int score2Value, int score3Value)
    {
        //write code here to set each class instance variable to the
        //values you passed to the constructor (average)
        Score1 = score1Value;
        Score2 = score2Value;
        Score3 = score3Value;
    } // end three-parameter constructor

    // calculate the average of the three scores
    public void computeAverage()
    {
        var sum = Score1 + Score2 + Score3;
        //Write the code to compute avg
        avg = sum / 3;
        //write the code to compute the remainder
        remainder = sum % 3;
    } // end method computeAverage




    // display the average, you fill in the rest of the code
    public void DisplayAverage()
    {
        //Write the console writeline to output the three variables,
        //the average and the remainder to the console
        Console.WriteLine("The average of " + Score1
                + ", " + Score2 + ", " + Score3 + " is "
                + avg + " with a remainder of " + remainder);
    } // end method DisplayAverage 
} // end class average
