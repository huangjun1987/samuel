﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2a
{
    public class averageTest
    {
        public static void Main(string[] args)
        {

            //instantiate a new average object here, passing the
            //the score1, score2 and score3 values respectively 
            int score1 = EnterScore();
            int score2 = EnterScore();
            int score3 = EnterScore();
            var myaverage = new average(score1, score2, score3);
            //call myaverage.computeAverage
            myaverage.computeAverage();
            //call myaverage.DisplayAverage()
            myaverage.DisplayAverage();
            Console.WriteLine(); // output a new line
            Console.ReadLine();
        } // end Main

        private static int EnterScore()
        {
            Console.Write("Enter an integer score ");
            var scoreStr = Console.ReadLine();
            if (int.TryParse(scoreStr, out int score))
            {
                return score;
            }
            else
            {
                Console.WriteLine("Ivalid entry. Please try again!");
                return EnterScore();
            }
        }

    } // end class averageTest

}
