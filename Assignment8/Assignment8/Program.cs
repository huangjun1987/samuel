﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment8
{
    class Program
    {
        private static int seed = 28;
        private static Random rand = new Random(seed);
        static void Main(string[] args)
        {
            int[] randomvalues = new int[10];
            int[] enteredvalues = new int[10];

            for (int i = 0; i < 10; i++)
            {
                randomvalues[i] = getRandom();
            }
            for (int i = 0; i < 10; i++)
            {
                enteredvalues[i] = EnterInt();
            }
            for (int i = 0; i < 10; i++)
            {
                if(enteredvalues[i]<randomvalues[i])
                {
                    Console.WriteLine($"The entered number {enteredvalues[i]} is less than {randomvalues[i]}");
                }
                else if (enteredvalues[i] > randomvalues[i])
                {
                    Console.WriteLine($"The entered number {enteredvalues[i]} is greater than {randomvalues[i]}");
                }
                else
                {
                    Console.WriteLine($"The entered number {enteredvalues[i]} is equal to {randomvalues[i]}");
                }
            }

            Console.ReadLine();
        }
        private static int getRandom()
        {         
            return rand.Next(1, 100);
        }
        private static int EnterInt()
        {
            Console.Write("Enter an integer number between 1 and 100: ");
            var valueStr = Console.ReadLine();
            if (int.TryParse(valueStr, out int value))
            {
                if (value < 1 || value > 100)
                {

                    Console.WriteLine("Ivalid entry. Please try again!");
                    return EnterInt();
                }
                return value;
            }
            else
            {
                Console.WriteLine("Ivalid entry. Please try again!");
                return EnterInt();
            }
        }
    }
}
