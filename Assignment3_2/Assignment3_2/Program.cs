﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment3_2
{
    class Program
    {
        static void Main(string[] args)
        {
            var hourlyPay = EnterHourlyPay();
            var hoursWorked = EnterHoursWorked();
            var grossPay = CalculateGrossPay(hoursWorked, hourlyPay);
            var netPay = CalucateNetPay(grossPay);

            Console.WriteLine($"Gross pay is {grossPay.ToString("c2")} and net pay is {netPay.ToString("c2")}");

            Console.ReadLine();
        }

        private static decimal CalucateNetPay(decimal grossPay)
        {
            decimal withholding;
            if (grossPay>1000)
            {
                withholding= 0.28M * grossPay;
            }
            else if(grossPay > 600)
            {
                withholding= 0.21M * grossPay;
            }
            else
            {
                withholding= 0.1M * grossPay;
            }
            return grossPay - withholding;
        }

        private static decimal CalculateGrossPay(int hoursWorked, decimal hourlyPay)
        {
            if (hoursWorked <= 40)
            {
                return hoursWorked * hourlyPay;
            }
            else
            {
                return 40 * hourlyPay + 1.5M * (hoursWorked - 40) * hourlyPay;
            }
        }

        private static int EnterHoursWorked()
        {
            Console.Write("Enter hours worked ");
            var valueStr = Console.ReadLine();
            if (int.TryParse(valueStr, out int value))
            {
                return value;
            }
            else
            {
                Console.WriteLine("Ivalid entry. Please try again!");
                return EnterHoursWorked();
            }
        }
        private static decimal EnterHourlyPay()
        {
            Console.Write("Enter hourly pay rate ");
            var valueStr = Console.ReadLine();
            if (decimal.TryParse(valueStr, out decimal value))
            {
                return value;
            }
            else
            {
                Console.WriteLine("Ivalid entry. Please try again!");
                return EnterHourlyPay();
            }
        }
    }
}
