﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment3
{
    class Program
    {
        static void Main(string[] args)
        {
            var value1 = EnterValue();
            var value2 = EnterValue();

            var changeValue = new ChangeValue(value1, value2);
            changeValue.printit();
        }
        private static int EnterValue()
        {
            Console.Write("Enter an integer value: ");
            var valueStr = Console.ReadLine();
            if (int.TryParse(valueStr, out int value))
            {
                return value;
            }
            else
            {
                Console.WriteLine("Ivalid entry. Please try again!");
                return EnterValue();
            }
        }
    }
}
