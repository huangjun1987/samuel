﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment3
{
    public class ChangeValue
    {
        public int value1;
        public int value2;
        public ChangeValue(int v1, int v2)
        {
            if (v1 > 5)
            {
                value1 = v1;
            }
            else
            {
                value1 = v1 + v2;
            }
            if (v2 < 10)
            {
                value2 = v2 * v2 + 5;
            }
            else
            {
                value2 = v2;
            }
        }

        public void printit()
        {
            Console.WriteLine($"The calculated value is: {value1 * value2}");
        }
    }
}
