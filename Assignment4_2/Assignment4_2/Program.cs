﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4_2
{
    class Program
    {
        const double COMMRATE1 = 0.10;
        const double COMMRATE2 = 0.20;
        const double COMMRATE3 = 0.30;
        static void Main(string[] args)
        {
            int salesmanNumber;
            do
            {
                salesmanNumber = EnterSalesmanNumber();
                if (salesmanNumber != 0)
                {
                    var sales = EnterSales(salesmanNumber);
                }
               
            }
            while (salesmanNumber != 0);

            Console.WriteLine("End program.");
            Console.Read();
        }
        private static void DisplayPay(int salesmanNumber, double sales)
        {
            double pay = 0;
            if (sales >3000)
            {
                Console.WriteLine("Invalid sales entry");
                pay = 0;
            }
            else if (sales >2000)
            {
                pay = sales * COMMRATE3;
            }
            else if (sales > 1000)
            {
                pay = sales * COMMRATE2;
            }
            else if (sales > 500)
            {
                pay = sales * COMMRATE1;
            }
            else 
            {
                pay = sales;
            }

            if (pay > 0)
            {
                Console.WriteLine("$The pay for salesman #{salesmanNumber} is {pay}");
            }
        }
        private static double EnterSales(int salesmanNumber)
        {
            Console.Write($"enter sales amount for employee {salesmanNumber}");
            var valueStr = Console.ReadLine();
            if (double.TryParse(valueStr, out double value))
            {
                return value;
            }
            else
            {
                Console.WriteLine("Ivalid entry. Please try again!");
                return EnterSales(salesmanNumber);
            }
        }
        private static int EnterSalesmanNumber()
        {
            Console.Write("enter sales # for employee ");
            var valueStr = Console.ReadLine();
            if (int.TryParse(valueStr, out int value))
            {
                return value;
            }
            else
            {
                Console.WriteLine("Ivalid entry. Please try again!");
                return EnterSalesmanNumber();
            }
        }
    }
}
